"use strict";


function bingFetch(point1, point2, key) {
    const base = "https://dev.virtualearth.net/REST/v1/Routes/Driving";
    const params = {
        key,
        "waypoint.1"    : point1.join(","),
        "waypoint.2"    : point2.join(","),
        optimize        : "timeWithTraffic",
        istanceUnit     : "Mile",
        maxSolutions    : "3",
        routeAttributes : "routePath",
    };
    const url = generateUrl(base, params);
    const fun = x => x.resourceSets[0].resources.map(x => bingRoute(x));
    return fetch(url).then(x => x.json()).then(x => fun(x));
}


function bingRoute(resource) {
    console.log(resource);
    return {
        duration   : resource.travelDurationTraffic,
        delay      : resource.travelDurationTraffic - resource.travelDuration,
        lineString : resource.routePath.line.coordinates,
    };
}
