"use strict";


initialize();


// ////////////////////////////////////////////////////////////////////////////
// Easy HTML access
// ////////////////////////////////////////////////////////////////////////////

function getBingKey()    { return localStorage.getItem("bingKey"); }
function setBingKey(key) { localStorage.setItem("bingKey", key); }
function qsBingKey()     { return document.querySelector("#bingKey"); }
function qsRouteName()   { return document.querySelector("#routeName"); }
function qsExit()        { return document.querySelector("#exit"); }
function qsResults()     { return document.querySelector("#results"); }
function qsRefresh()     { return document.querySelector("#refresh"); }


// ////////////////////////////////////////////////////////////////////////////
// Initialization
// ////////////////////////////////////////////////////////////////////////////

function initialize() {
    initializeStore();
    initializeRefresh();
}


function initializeStore() {
    qsBingKey().value = getBingKey();
    document.querySelector("#store").addEventListener("click", handleStore);
}


function initializeRefresh() {
    const fun = (x, i) => elem("option", x.name, [], {value: i});
    const children = WAYPOINTS.map((x, i) => fun(x, i));
    qsRouteName().replaceChildren(...children);
    replaceExits();
    qsRefresh().addEventListener("click", handleRefresh);
    qsRouteName().addEventListener("change", replaceExits);
}


function replaceExits() {
    const items = WAYPOINTS[qsRouteName().value].items;
    const children = items.filter(x => x.exit !== "XXX").map((x, i) => exitOption(x, i));
    qsExit().replaceChildren(...children);
}


// ////////////////////////////////////////////////////////////////////////////
// Event Handlers
// ////////////////////////////////////////////////////////////////////////////

function handleStore() {
    const value = qsBingKey().value;
    (value !== "") && setBingKey(value);
}


function exitOption({exit, description}, i) {
    const text = "Exit " + exit + " - " + description;
    return elem("option", text, [], {value: i})
}


function handleRefresh() {
    qsResults().replaceChildren([]);
    qsRefresh().setAttribute("disabled", true);
    const items = WAYPOINTS[qsRouteName().value].items;
    const i1 = Number(qsExit().value);
    const i2 = Math.min(i1 + 3, items.length - 1);
    cascadeFetch(items, i1, i2, "first");
}


// ////////////////////////////////////////////////////////////////////////////
// Cascade fetch of traffic route data
// ////////////////////////////////////////////////////////////////////////////

function cascadeFetch(items, i1, i2, prev) {
    const promise = bingFetch(items[i1].point, items[i2].point, getBingKey());
    promise.then(x => cascadeFulfilled(items, i1, i2, prev, x))
        .catch(x => cascadeRejected(items, i1, i2, prev, x));
}


function cascadeFulfilled(items, i1, i2, prev, result) {
    const points = items.slice(i1 + 1, i2).map(x => x.point);
    const routes = result.map(x => ({...x, detour: detourType(x, points)}))
        .sort((x, y) => x.duration - y.duration)
        .filter(x => prev === "detour" ? x.detour !== "partial" : true);
    const children = routes.map(routeHref).map(x => elem("div", "", [x]));
    appendExitDetails(items[i1], "", children);
    if (i1 > 0 && routes.filter(x => x.detour !== "none") > 0) {
        cascadeFetch(items, i1 - 1, i2, "detour");
    } else {
        qsRefresh().removeAttribute("disabled");
    }
}


function cascadeRejected(items, i1, i2, prev, reason) {
    console.log(reason);
    appendExitDetails(items[i1], reason.toString(), []);
    if (i1 > 0 && prev !== "error") {
        cascadeFetch(items, i1 - 1, i2, "error");
    } else {
        qsRefresh().removeAttribute("disabled");
    }
}


function detourType(route, points) {
    const count = midpointCount(route.lineString, points, 0);
    return count === 0 ? "full" : (count === points.length ? "none" : "partial");
}


function midpointCount(lineString, points, count) {
    if (points.length === 0) { return count; }
    const i = midpointIndex(lineString, points[0]);
    if (i === -1) { return count; }
    return midpointCount(lineString.slice(i), points.slice(1), count + 1);
}


function midpointIndex(lineString, point, tolerance=10) {
    const slice = lineString.slice(0, -1);
    const fun = (x, i) => crossTrack(point, x, lineString[i + 1]);
    return slice.findIndex((x, i) => Math.abs(fun(x, i)) <= tolerance);
}


// ////////////////////////////////////////////////////////////////////////////
// Generate HTML Elements for results
// ////////////////////////////////////////////////////////////////////////////

function elem(name, text, children=[], attrs={}) {
    const e = document.createElement(name);
    e.append(text, ...children);
    Object.entries(attrs).map(x => e.setAttribute(x[0], x[1]));
    return e;
}


function appendExitDetails(item, text, children) {
    const summary = elem("summary", `Exit ${item.exit} - ${item.description}`);
    const details = elem("details", text, [summary, ...children], {open: "true"});
    qsResults().append(details);
}


function routeHref({duration, lineString, detour}) {
    const prefix = detour === "none" ? "Normal" : "DETOUR";
    const text = prefix + " - " + (duration / 60).toFixed(1) + " minutes";
    const href = googleUrlDirections(lineString);
    return elem("a", text, [], {href});
}


// ////////////////////////////////////////////////////////////////////////////
// Miscellaneous
// ////////////////////////////////////////////////////////////////////////////

function winnowArray(arr, num) {
    const step = arr.length / num;
    const fun = (x, i) => (0.5 + x.length) * step <= (i + 1);
    return arr.reduce((x, y, i) => fun(x, i) ? [...x, y] : x, []);
}


function generateUrl(base, params) {
    params = Object.entries(params);
    params = params.map(x => x.map(encodeURIComponent).join("=")).join("&");
    return base + "?" + params;
}


function googleUrlDirections(lineString, wpNum=3) {
    const waypoints = winnowArray(lineString.slice(1, -1), wpNum).join("|");
    const params = {
        waypoints,
        api         : "1",
        origin      : lineString[0],
        destination : lineString.at(-1),
    };
    return generateUrl("https://www.google.com/maps/dir/", params);
}


// ////////////////////////////////////////////////////////////////////////////
// Latitude and Longitude manipulations
// ////////////////////////////////////////////////////////////////////////////

function radians(degree) {
    return degree / 180 * Math.PI;
}


function haversine([lat1, lng1], [lat2, lng2], radius) {
    const dLat    = radians(lat2) - radians(lat1);
    const dLng    = radians(lng2) - radians(lng1);
    const a       = Math.sin(dLat / 2) ** 2 + Math.cos(radians(lat1)) * Math.cos(radians(lat2)) * Math.sin(dLng / 2) ** 2;
    const c       = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return radius * c;
}


function initialBearing([lat1, lng1], [lat2, lng2]) {
    const dLng = radians(lng2 - lng1);
    const x = Math.cos(radians(lat1)) * Math.sin(radians(lat2)) - Math.sin(radians(lat1)) * Math.cos(radians(lat2)) * Math.cos(dLng);
    const y = Math.sin(dLng) * Math.cos(radians(lat2));
    return Math.atan2(y, x);
}


function crossTrack([lat0, lng0], [lat1, lng1], [lat2, lng2]) {
    const R = 6371e3;
    const dist = haversine([lat0, lng0], [lat1, lng1], R);
    if (lat1 === lat2 && lng1 === lng2) { return dist; };
    const bear10 = initialBearing([lat1, lng1], [lat0, lng0]);
    const bear12 = initialBearing([lat1, lng1], [lat2, lng2]);
    return R * Math.asin(Math.sin(dist / R) * Math.sin(bear10 - bear12));
}
